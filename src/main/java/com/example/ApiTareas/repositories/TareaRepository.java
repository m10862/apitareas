package com.example.ApiTareas.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ApiTareas.models.Tarea;

@Repository
public interface TareaRepository extends JpaRepository<Tarea, Integer>{

	
}
