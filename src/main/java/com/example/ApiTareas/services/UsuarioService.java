package com.example.ApiTareas.services;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.example.ApiTareas.models.Usuario;
import com.example.ApiTareas.repositories.UsuarioRepository;


@Service
public class UsuarioService implements BaseService<Usuario>{

	private UsuarioRepository usuarioRepository;
	
	public UsuarioService(UsuarioRepository usuarioRepository) {
		this.usuarioRepository = usuarioRepository;
	}
	
	@Override
	@Transactional
	public List<Usuario> findAll() throws Exception {
		try {
			List<Usuario> entities = usuarioRepository.findAll();
			return entities;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	@Transactional
	public Usuario findById(Integer id) throws Exception {
		try {
			Optional<Usuario> entityOptional = usuarioRepository.findById(id);
			return entityOptional.get();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	@Transactional
	public Usuario saveAndFlush(Usuario entity) throws Exception {
		try {
			entity = usuarioRepository.saveAndFlush(entity);
			return entity;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	@Transactional
	public Usuario update(Integer id, Usuario entity) throws Exception {
		try {
			Optional<Usuario> entityOptional = usuarioRepository.findById(id);
			Usuario usuario = entityOptional.get();
			usuario = usuarioRepository.save(entity);
			return usuario;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	@Transactional
	public boolean delete(Integer id) throws Exception {
		try {
			if (usuarioRepository.existsById(id)) {
				usuarioRepository.deleteById(id);
				return true;
			}else {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
