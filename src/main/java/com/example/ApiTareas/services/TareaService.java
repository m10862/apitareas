package com.example.ApiTareas.services;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.example.ApiTareas.models.Tarea;
import com.example.ApiTareas.repositories.TareaRepository;

@Service
public class TareaService implements BaseService<Tarea>{

	private TareaRepository tareaRepository;
	
	public TareaService(TareaRepository tareaRepository) {
		this.tareaRepository = tareaRepository;
	}
	
	@Override
	public List<Tarea> findAll() throws Exception {
		try {
			List<Tarea> entities = tareaRepository.findAll();
			return entities;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public Tarea findById(Integer id) throws Exception {
		try {
			Optional<Tarea> entityOptional = tareaRepository.findById(id);
			return entityOptional.get();
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public Tarea saveAndFlush(Tarea entity) throws Exception {
		try {
			entity = tareaRepository.saveAndFlush(entity);
			return entity;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public Tarea update(Integer id, Tarea entity) throws Exception {
		try {
			Optional<Tarea> entityOptional = tareaRepository.findById(id);
			Tarea tarea = entityOptional.get();
			tarea = tareaRepository.save(entity);
			return tarea;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	public boolean delete(Integer id) throws Exception {
		try {
			if (tareaRepository.existsById(id)) {
				tareaRepository.deleteById(id);
				return true;
			}else {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
}
