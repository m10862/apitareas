package com.example.ApiTareas.models;

import java.io.Serializable;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "TAREAS")
public class Tarea implements Serializable{
private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "IDTAREA")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idtarea;
	
	@Column(name = "NOMBRE")
	private String nombre;
	
	@Column(name = "FECHA")
	@Temporal(TemporalType.DATE)
	private Calendar fecha;
	
	@Column(name = "PRIORIDAD")
	private String prioridad;
	
	@Column(name = "ESTATUS")
	private String estatus;
	
	@ManyToOne
	@JoinColumn(name = "ID_ASIGNADO")
	private Usuario usuario;

	public Integer getIdtarea() {
		return idtarea;
	}

	public void setIdtarea(Integer idtarea) {
		this.idtarea = idtarea;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Calendar getFecha() {
		return fecha;
	}

	public void setFecha(Calendar fecha) {
		this.fecha = fecha;
	}

	public String getPrioridad() {
		return prioridad;
	}

	public void setPrioridad(String prioridad) {
		this.prioridad = prioridad;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
}
