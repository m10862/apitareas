package com.example.ApiTareas.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "USUARIOS")
public class Usuario implements Serializable{
private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "IDUSUARIO")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idusuario;
	@Column(name = "NOMBRE")
	private String nombre;
	@Column(name = "EMAIL")
	private String email;
	@Column(name = "NUMERO")
	private long numero;
	@Column(name = "EXT")
	private Integer ext;
	
	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL)
	private List<Tarea> tarea;
	
	public Integer getIdusuario() {
		return idusuario;
	}
	public void setIdusuario(Integer idusuario) {
		this.idusuario = idusuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getNumero() {
		return numero;
	}
	public void setNumero(long numero) {
		this.numero = numero;
	}
	public Integer getExt() {
		return ext;
	}
	public void setExt(Integer ext) {
		this.ext = ext;
	}
	
	


}
