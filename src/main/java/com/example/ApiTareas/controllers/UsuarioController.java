package com.example.ApiTareas.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.ApiTareas.models.Usuario;
import com.example.ApiTareas.services.UsuarioService;

@RestController
@RequestMapping(path = "api/v1/usuario")
public class UsuarioController {
	private UsuarioService usuarioService;
	
	public UsuarioController(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@GetMapping(path = "/listar", produces = {"application/json"})
	public ResponseEntity<?> getAll() {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.findAll());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("{\"error\":\"Error.Por favor intente mas tarde.\"}");
		}
	}
	
	@GetMapping(path = "/buscar/{id}", produces = {"application/json"})
	public ResponseEntity<?> getOne(@PathVariable Integer id) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.findById(id));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("{\"error\":\"Error.Por favor intente mas tarde.\"}");
		}
	}
	
	@PostMapping(path = "/insetar", produces = {"application/json"})
	public ResponseEntity<?> saveAndFlush(@RequestBody Usuario entity) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.saveAndFlush(entity));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("{\"error\":\"Error.Por favor intente mas tarde.\"}");
		}
	}
	
	@PutMapping(value = "/actualizar/{id}", produces = {"application/json"})
	public ResponseEntity<?> update(@PathVariable Integer id, @RequestBody Usuario entity){
		try {
			return ResponseEntity.status(HttpStatus.OK).body(usuarioService.update(id, entity));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("{\"error\":\"Error.Por favor intente mas tarde.\"}");
		}
	}
	
	@DeleteMapping(path = "/eliminar/{id}", produces = {"application/json"})
	public ResponseEntity<?> delete(@PathVariable Integer id) {
		try {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(usuarioService.delete(id));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("{\"error\":\"Error.Por favor intente mas tarde.\"}");
		}
	}
}
